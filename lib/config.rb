module Pagination

  class << self
    def configure
      yield config()
    end

    def config
      @_config ||= Config.new
    end
  end

  class Config
    attr_accessor :default_per_page, :window, :param_name, :page_method_name

    def initialize
      @default_per_page = 25
      @window = 4
      @param_name = :page #参数名
      @page_method_name = :page #分页方法名
    end

  end
end