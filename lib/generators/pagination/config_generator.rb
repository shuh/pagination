module Pagination
  module Generators
    #bin/rails g pagination:config
    class ConfigGenerator < Rails::Generators::Base
      #指定template source的目录
      source_root File.expand_path(File.join(File.dirname(__FILE__ ), 'templates'))
      desc <<DESC
复制配置文件到你的项目中去。
DESC
      def copy_config_file
        # template "pagination_config.rb", 'config/initializers/pagination_config.rb'
        copy_file 'pagination_config.rb', 'config/initializers/pagination_config.rb'
      end
    end
  end
end