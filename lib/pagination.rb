require "pagination/version"

module Pagination
  class Error < StandardError; end
  # Your code goes here...
end

# load Rails/Railtie
# begin
#   require 'rails'
# rescue LoadError
#   #do nothing
# end

require 'config'
require 'pagination/activerecord'
require 'pagination/actionview'
require 'pagination/helpers/paginator'