require 'pagination/version'
require 'active_support/lazy_load_hooks'

#调用model的时候加载
ActiveSupport.on_load :active_record do
  require 'pagination/activerecord/active_record_extension'
  ::ActiveRecord::Base.send :include, Pagination::ActiveRecordExtension
end