require 'pagination/activerecord/active_record_model_extension'
module Pagination
  module ActiveRecordExtension
    extend ActiveSupport::Concern

    #定义ClassMethods，里面的代码会自动加载成类方法
    module ClassMethods
      #扩展将来的子类
      def inherited(kls)
        super
        kls.send(:include, Pagination::ActiveRecordModelExtension) if kls.superclass == ::ActiveRecord::Base
      end
    end

    included do
      #扩展现有的子类
      descendants.each do |kls|
        kls.send(:include, Pagination::ActiveRecordModelExtension) if kls.suppress == ::ActiveRecord::Base
      end
    end

  end
end