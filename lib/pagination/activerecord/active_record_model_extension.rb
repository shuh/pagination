require 'pagination/activerecord/active_record_relation_extension'
module Pagination
  module ActiveRecordModelExtension
    extend ActiveSupport::Concern

    module ClassMethods
      def default_per_page
        return @_default_per_page if defined?(@_default_per_page) && @_default_per_page
        @_default_per_page = Pagination.config.default_per_page
      end
    end

    included do
      # Model.page(5)
      eval <<-RUBY, nil, __FILE__, __LINE__ + 1
        def self.#{Pagination.config.page_method_name}(num=nil)
          num = num.to_i
          num = 1 if num <= 0
          per = Pagination.config.default_per_page
          limit(per).offset(per * (num - 1)).extending do
            include Pagination::ActiveRecordRelationExtension
          end
        end
      RUBY
    end
  end
end