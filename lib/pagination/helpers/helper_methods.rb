require 'active_support/inflector'
require 'pagination/helpers/tags'
module Pagination
  module Helpers

    module HelperMethods
      # A helper that renders the pagination links.
      #
      #   <%= paginate @articles %>
      #
      # ==== Options
      # * <tt>:window</tt> - The "inner window" size (4 by default).
      # * <tt>:outer_window</tt> - The "outer window" size (0 by default).
      # * <tt>:left</tt> - The "left outer window" size (0 by default).
      # * <tt>:right</tt> - The "right outer window" size (0 by default).
      # * <tt>:params</tt> - url_for parameters for the links (:controller, :action, etc.)
      # * <tt>:param_name</tt> - parameter name for page number in the links (:page by default)
      # * <tt>:remote</tt> - Ajax? (false by default)
      # * <tt>:paginator_class</tt> - Specify a custom Paginator (Kaminari::Helpers::Paginator by default)
      # * <tt>:template</tt> - Specify a custom template renderer for rendering the Paginator (receiver by default)
      # * <tt>:ANY_OTHER_VALUES</tt> - Any other hash key & values would be directly passed into each tag as :locals value.
      def paginate(scope, paginator_class: Pagination::Helpers::Paginator, template: nil, **options)
        template ||= self
        options[:total_pages] ||= scope.total_pages
        options.reverse_merge! current_page_num: scope.current_page,
                               prev_page: scope.limit_value,
                               remote: false

        paginator = paginator_class.new(template, **options)
        paginator.to_s
      end

      def page_entries_info(collection, entry_name: nil)
        # 518件中の1～10件の商品を表示しています。
        entry_name ||= collection.entry_name
        from = collection.offset_value + 1
        to = collection.offset_value + (collection.respond_to?(:records) ? collection.records : collection.to_a).size
        <<-EOF
          <div>#{collection.total_count}件中の#{from}～#{to}件の#{entry_name}を表示しています。</div>
        EOF
          .html_safe
      end
    end

  end
end