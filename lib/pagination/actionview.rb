require 'pagination/version'
require 'active_support/lazy_load_hooks'

#项目启动的时候就会加载
ActiveSupport.on_load :action_view do
  require 'pagination/helpers/helper_methods'
  ::ActionView::Base.send(:include, Pagination::Helpers::HelperMethods)
end